var template;

$('document').ready(function () {
    updateApp();
});

$(window).resize(function () {
    updateApp(); 
});

function updateApp() {
    var data = {greeting: "Hello World"};

    if (template === undefined) {
        $.get("/tmpl/greeting.html", function (value) {
            var template = $.templates(value);
            var htmlOutput = template.render(data);
            $("#app").html(htmlOutput);
        });
    } else {
        var htmlOutput = template.render(data);
        $("#app").html(htmlOutput);
    }
}