# Hello World in JsRender

A tiny project to demo how to use JsRender.

## Usage

Run it with a developmental HTTP server, e.g. `http.server` module in Python 3:

```
$ python -m http.server
```

[Here](https://gist.github.com/willurd/5720255) is a list of HTTP server one-liner.

## Copyright

2018. Michael Chen; this repo is licensed under Apache 2.0.